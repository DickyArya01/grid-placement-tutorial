using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// scriptable object untuk menyimpan list data objek (database)

[CreateAssetMenu]
public class ObjectsDatabaseSO : ScriptableObject 
{
    public List<ObjectData> listObjectData;
}
