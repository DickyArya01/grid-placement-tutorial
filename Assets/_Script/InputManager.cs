using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    private Camera sceneCamera;

    private Vector3 lastPosition;

    [SerializeField]
    private LayerMask placementLayermask;

    public event Action OnClicked, OnExit; //untuk invoke sebuah method, yang mana bisa di isi dengan method yang diperlukan

    /// <summary>
    /// menjalankan method invoke dari Action OnClicked dan OnExit
    /// </summary>
    void Update()
    {
       if (Input.GetMouseButtonDown(0))
            OnClicked?.Invoke();

        if (Input.GetKeyDown(KeyCode.Space))
            OnExit?.Invoke();        
    }

    /// <summary>
    /// return bool ketika pointer berada di atas ui atau tidak
    /// </summary>
    public bool IsPointerOverUI()
        => EventSystem.current.IsPointerOverGameObject();

    /// <summary>
    /// return posisi mouse dari pointer berdasarkan raycast dari kamera ke layer placementLayermask.
    /// <para> mousePos.z = sceneCamera.neaAClipPlane, Dengan menetapkan posisi Z mouse ke nilai nearClipPlane, maka titik cursor akan diubah ke posisi dalam koordinat 3D yang sesuai dengan lokasi yang sama dengan posisi mouse di layar.</para>
    /// <para> Ray ray = sceneCamera.ScreenPointToRay(mousePos), inisiasi ray dengan nilai ray dari kamera ke posisi mouse </para>
    /// </summary>
    public Vector3 GetSelectedMapPosition()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = sceneCamera.nearClipPlane;
        Ray ray = sceneCamera.ScreenPointToRay(mousePos);
        RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit, 100, placementLayermask))
        {
            lastPosition = hit.point; 
            Debug.DrawLine(sceneCamera.gameObject.transform.position, hit.point, Color.yellow);
        }
        return lastPosition;
    }
}
