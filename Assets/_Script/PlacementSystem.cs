using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementSystem : MonoBehaviour
{
    [SerializeField]
    GameObject mouseIndicator, cellIndicator;
    [SerializeField]
    private InputManager inputManager;
    [SerializeField]
    private Grid grid;

    [SerializeField]
    private ObjectsDatabaseSO database;
    private int selectedObjectIndex = -1;

    [SerializeField]
    private GameObject gridVisualization;

    void Start()
    {
        StopPlacement();
    }

    /// <summary>
    /// Method untuk keluar dari fase placement, method ini dijalankan pada method OnClick di button UI
    /// <para>inisiasi selectedObjectIndex dari listObjectData di database yang ID data nya sama dengan ID yang dicari</para>
    /// <para>Aktifkan gameobject gridVisualization dan cellIndicator</para>
    /// <para>isi action OnClicked dengan method PlaceStructure dan isi action OnExit dengan method StopPlacement</para>
    /// </summary>
    public void StartPlacement(int ID)
    {
        StopPlacement();
        selectedObjectIndex = database.listObjectData.FindIndex(data => data.ID == ID);
        Debug.Log(selectedObjectIndex);

        if (selectedObjectIndex < 0)
        {
           Debug.LogError($"No ID Found {ID}"); 
           return;
        }

        gridVisualization.SetActive(true);
        cellIndicator.SetActive(true);
        inputManager.OnClicked += PlaceStructure;
        inputManager.OnExit += StopPlacement;
    }

    /// <summary>
    /// Method untuk menaruh gameobject dalam grid
    /// <para>jika pointer diatas ui, tidak melakukan apa-apa</para>
    /// <para>instantiate prefab sesuai index, kemudian taruh posisinya di gridposition</para>
    /// </summary>
    private void PlaceStructure()
    {
        if (inputManager.IsPointerOverUI())
        {
            return;
        }

        Vector3 mousePosition = inputManager.GetSelectedMapPosition();
        Vector3Int gridPosition = grid.WorldToCell(mousePosition);
        GameObject newObject = Instantiate(database.listObjectData[selectedObjectIndex].Prefab);
        newObject.transform.position = grid.CellToWorld(gridPosition);
    }

    /// <summary>
    /// Method untuk keluar dari fase placement
    /// <para>inisiasi index objek yang dipilih = -1 (tidak ada yang dipilih)</para>
    /// <para>Non-aktifkan gameobject gridVisualization dan cellIndicator</para>
    /// <para>Hapus isi method pada Action OnClicked dan OnExit</para>
    /// </summary>
    public void StopPlacement()
    {
        selectedObjectIndex = -1;
        gridVisualization.SetActive(false);
        cellIndicator.SetActive(false);
        inputManager.OnClicked -= PlaceStructure;
        inputManager.OnExit -= StopPlacement;
    }

    void Update()
    {
        if (selectedObjectIndex < 0)
            return;

        Vector3 mousePosition = inputManager.GetSelectedMapPosition();
        Vector3Int gridPosition = grid.WorldToCell(mousePosition);
        mouseIndicator.transform.position = mousePosition;
        cellIndicator.transform.position = grid.CellToWorld(gridPosition);
    }
}
